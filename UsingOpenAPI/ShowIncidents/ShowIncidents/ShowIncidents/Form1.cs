﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RestSharp;

namespace ShowIncidents
{
    public partial class Forms1 : Form
    {
        public Forms1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken, MensajeFinalMostar = "", MensajeFinalMostar2 = "";
            int j = 0, z = 41, zTime = z + 3;
            string numLine; //If the user introduces a number very long
            email = this.textBox1.Text;
            passwd = this.textBox2.Text;
            numLine = this.Number1.Text;

            MensajeFinalMostar2 = "The BusLine " + numLine + " have Incidents: " + Environment.NewLine + Environment.NewLine;
            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);


            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);


            if (response.Content[10] == compr[0])
            {
                //ss
                //List all LineBuses
                numToken = words[19];
                var client2 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/info/20180921/");
                var request2 = new RestRequest(Method.GET);

                request2.AddHeader("accessToken", numToken);
                IRestResponse response2 = client2.Execute(request2);

                string text2 = response2.Content;
                string[] words2 = text2.Split(delimiterChars);
                string AcceptPetition;


                AcceptPetition = words2[3];
                int lastLine = 0 ;
                string ultimaLinea;
                bool encontrado = false;
                if (AcceptPetition == "00")
                {

                    ultimaLinea = words2[lastLine];

                    while (ultimaLinea != "datetime")
                    {
                        lastLine++;
                        ultimaLinea = words2[lastLine];
                    }
                    lastLine = lastLine - 11; //this is the position where we have the last BusLine
                    ultimaLinea = words2[lastLine];
                    int buscLinea = 25;

                    while (buscLinea < lastLine && !encontrado)
                    {
                        if(numLine == words2[buscLinea])
                        {
                            encontrado = true;
                        }
                        else
                        {
                            buscLinea = buscLinea + 28;
                        }
        //              string most = words2[buscLinea];
                     
                    }

                 if (encontrado)
                    {
        //              MessageBox.Show("parada existe");

                        //HERE work on requesting stop information
                        string url = "https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/incidents/" + numLine + "/";
                        var client3 = new RestClient(url);
                        var request3 = new RestRequest(Method.GET);

                        request3.AddHeader("accessToken", numToken);
                        IRestResponse response3 = client3.Execute(request3);

        //              MessageBox.Show(response3.Content);

                        string text3 = response3.Content;
                        string[] words3 = text3.Split(delimiterChars);

                        string MensajeFinal, NoBuses = ": [], ";
                        // if don´t exist a stop between the stop 1 and the last stop 7396 Es para ver si no hay alguna parada entre medias de 0 y la final

                        
                        NoBuses = words3[j];
                        while (NoBuses != "item")
                        {
                            j++;
                            NoBuses = words3[j];//153
                        }
                        j++; //pos que dice si existe incidencia en esa linea

                        NoBuses = words3[j];

                        if (NoBuses == ": [], ")//The BusLine doesn´t exist
                        {
                            j = -1;
                        }

                        if (j == -1)
                        {
                            richTextBox1.Text = "The BusLine " + numLine +" doesn´t have Incidents";
                        }
                        else {
                            // Here catch the position where we have last Bus
                            int v = j;

                            MensajeFinal = words3[v];
                            
                
                            while (MensajeFinalMostar != "titleAcabado")
                            {
                                while (MensajeFinal != "title")
                                {

                                    if (MensajeFinal == "ttl")
                                    {
                                        MensajeFinalMostar = "titleAcabado";
                                        MensajeFinal = "title";
                                    }
                                    else
                                    {
                                        v++;
                                        MensajeFinal = words3[v];
                                       
                                    }
                                }
                                if (words3[v + 2] != "120")
                                {
                                    MensajeFinalMostar2 = MensajeFinalMostar2 + words3[v + 2] + Environment.NewLine;
                                    string Fecha1 = "", Fecha2 = "";
                                    int buscInicio = v, buscFinal = v;
                                    while (words3[buscInicio] != "rssAfectaDesde")
                                    {
                                        buscInicio--;
                                    }
                                    Fecha1 = words3[buscInicio + 2];

                                    while (words3[buscFinal] != "rssAfectaHasta")
                                    {
                                        buscFinal++;
                                    }
                                    Fecha2 = words3[buscFinal + 2];

                                    MensajeFinalMostar2 = MensajeFinalMostar2 + Fecha1 + " - " + Fecha2 + Environment.NewLine + Environment.NewLine;

                                }

                                v++;//this is the position where have last bus in the json. pos del ultimo bus en pasar por esa parada
                                MensajeFinal = words3[v];
                               
                            }          //}

                            richTextBox1.Text = MensajeFinalMostar2;
                            //First Bus  to came to the stop
                            
                        }                       
                    }
                    else
                    {
                        //MessageBox.Show("The stop doesn´t exist");
                        richTextBox1.Text = "The BusLine doesn´t exist";
                    }

                    //            MessageBox.Show("correct");
                }
            }
            else
            {
                //MessageBox.Show("email  or password error");
                richTextBox1.Text = "email  or password error";
            }

        }

        private void Number1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
