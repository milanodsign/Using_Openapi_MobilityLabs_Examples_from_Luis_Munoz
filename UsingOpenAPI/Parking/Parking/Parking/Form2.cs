﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace Parking
{
    public partial class Form2 : Form
    {
        public string tokenrecibido;
        public int numeroDeParking = 0;

        public string[] ParkingNombre, ParkingNumero;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load_1(object sender, EventArgs e)
        {
            InicializoParking(tokenrecibido);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            object num = comboBox1.SelectedItem;
            string parkingSeleccionada = comboBox1.SelectedItem.ToString();
            string id = ParkingSeleccionado(parkingSeleccionada);
            Validation(tokenrecibido, id);
            comboBox1.Items.Clear();
            //comboBox1.ResetText();
            InicializoParking(tokenrecibido);
        }


        public void InicializoParking(string tokenr)
        {
        
            var client2 = new RestClient("https://openapi.emtmadrid.es/v1/citymad/places/parkings/EN/");
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            int lastPark = 0;
            string ultimoPark;
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            string AcceptPetition;



            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimoPark = words2[lastPark];
                while (ultimoPark != "description")
                {
                    lastPark++;
                    ultimoPark = words2[lastPark];
                }
               // lastPark = lastPark - 52; //this is the position where we have the last parking(name)
               while(words2[lastPark] != "name")
                {
                    lastPark--;//this is the position where we have the last parking(name)
                }

                ultimoPark = words2[lastPark];

                int posPark = 0;

                //ultimaCat = words2[posCat];
                while (posPark <= lastPark)
                {
                    if (ultimoPark == "name")
                    {
                        numeroDeParking++;
                    }
                    posPark++;
                    ultimoPark = words2[posPark];
                }
                posPark = 0;
                int meteDatos = 0;
               
                ParkingNombre = new string[numeroDeParking];
                ParkingNumero = new string[numeroDeParking];
               

                ultimoPark = words2[posPark];
                while (posPark <= lastPark)
                {
                    if (ultimoPark == "name")
                    {
                        ParkingNombre[meteDatos] = words2[posPark + 2];

                        while (words2[posPark] != "id")
                        {
                            posPark++;
                        }
                        ParkingNumero[meteDatos] = words2[posPark + 2];//id del parking

                        meteDatos++;
                    }
                    posPark++;
                    ultimoPark = words2[posPark];
                }

                int z = 0;
                while (z < meteDatos)
                {
                    comboBox1.Items.Add(ParkingNombre[z]);
                    z++;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        public string ParkingSeleccionado(string parkingSeleccionado)
        {
            string IdParking = "";

            int buscoParkingSeleccionada = 0;

            while (ParkingNombre[buscoParkingSeleccionada] != parkingSeleccionado)
            {
                buscoParkingSeleccionada++;//saco la pos en los array del parking seleccionado
            }
            IdParking = ParkingNumero[buscoParkingSeleccionada];// obtengo de la pos del array de Id el id de ese parking            

            return IdParking;
        }

        public void Validation(string tokenr, string id)
        {
            string url = "https://openapi.emtmadrid.es/v1/citymad/places/parking/" + id + "/EN/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            string AcceptPetition, nombre, horario1, horario2, direccion, PuestosLibres, mensaje = "";
            int contador = 0;
            

            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                while(words2[contador] != "name")
                {
                    contador++;
                }
                contador = contador + 2;
                nombre = words2[contador];//obtengo el name del parking

                while(words2[contador] != "schedule")
                {
                    contador++;
                }
                contador = contador + 2;
                horario1 = words2[contador];//obtengo el horario1 del parking

               /* while (words2[contador] != "scheduleEnd")
                {
                    contador++;
                }
                contador = contador + 2;
                horario2 = words2[contador];//obtengo el horario2 del parking*/

                while (words2[contador] != "occupation")
                {
                    contador++;
                }
                contador++;//pos de plazas libres
                if (words2[contador] != ": null, ")
                {
                    while (words2[contador] != "free")
                    {
                        contador++;
                    }
                    contador++;
                    string GuardarNumero = "";
                    char[] delimiterChars2 = { ':', '}' };
                    string[] words3;
                    GuardarNumero = words2[contador];
                    words3 = GuardarNumero.Split(delimiterChars2);
                    

                    PuestosLibres = words3[1];//obtengo el numero de plazas libres del parking
                }
                else
                {
                    PuestosLibres = "No disponible";
                }

                while (words2[contador] != "address")
                {
                    contador--;
                }
                contador = contador + 2;
                direccion = words2[contador];//obtengo el horario del parking

                mensaje = mensaje + nombre + ": " + Environment.NewLine;
                mensaje = mensaje + "Direccion:" + direccion + Environment.NewLine;
                mensaje = mensaje + "Horario: " + horario1 /*+ " - " + horario2 */+ Environment.NewLine;
                mensaje = mensaje + "Numero de plazas Libres: " + PuestosLibres + Environment.NewLine;

                richTextBox1.Text = mensaje;

            }
            else
            {
                richTextBox1.Text = "Se ha producido un error";
            }
        }
    }
}
