﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RestSharp;


namespace DetailsForDayInBusLine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
        }

        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken, MensajeFinalMostar = "", MensajeFinalMostar2 = "";
            int j = 0, z = 41, zTime = z + 3;
            string numLine, Date; //If the user introduces a number very long
            email = this.textBox1.Text;
            passwd = this.textBox2.Text;
            numLine = this.Number1.Text;
            //this.textBox4.Text = DateTime.ToString("dd/MM/yyyy");
            Date = this.textBox4.Text;

            Date = Date + ".";
            char[] delimiterCharsDate = { '/', '.' };
            string textDate = Date;
            string[] wordsDate = textDate.Split(delimiterCharsDate);

            string DateCorrect = "";
            int recorrerDate = 0;
            DateCorrect = wordsDate[recorrerDate];

            while (recorrerDate < 3)
            {
                recorrerDate++;
                DateCorrect = wordsDate[recorrerDate] + DateCorrect;

            }
            MensajeFinalMostar2 = "The BusLine " + numLine + " in the date " + Date +" :" + Environment.NewLine + Environment.NewLine;
            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);


            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);


            if (response.Content[10] == compr[0])
            {

                //List all LineBuses
                numToken = words[19];
                var client2 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/info/20180921/");
                var request2 = new RestRequest(Method.GET);

                request2.AddHeader("accessToken", numToken);
                IRestResponse response2 = client2.Execute(request2);

                string text2 = response2.Content;
                string[] words2 = text2.Split(delimiterChars);
                string AcceptPetition;


                AcceptPetition = words2[3];
                int lastLine = 0;
                string ultimaLinea;
                bool encontrado = false;
                if (AcceptPetition == "00")
                {

                    ultimaLinea = words2[lastLine];

                    while (ultimaLinea != "datetime")
                    {
                        lastLine++;
                        ultimaLinea = words2[lastLine];
                    }
                    lastLine = lastLine - 11; //this is the position where we have the last BusLine
                    ultimaLinea = words2[lastLine];
                    int buscLinea = 25;

                    while (buscLinea < lastLine && !encontrado)
                    {
                        if (numLine == words2[buscLinea])
                        {
                            encontrado = true;
                        }
                        else
                        {
                            buscLinea = buscLinea + 28;
                        }
                        //              string most = words2[buscLinea];

                    }

                    if (encontrado)
                    {
        //              MessageBox.Show("parada existe");

                        //HERE work on requesting stop information
                        string url = "https://openapi.emtmadrid.es/v1/transport/busemtmad/calendar/"+ DateCorrect + "/" + DateCorrect + "/";
                        var client3 = new RestClient(url);
                        var request3 = new RestRequest(Method.GET);

                        request3.AddHeader("accessToken", numToken);
                        IRestResponse response3 = client3.Execute(request3);

        //              MessageBox.Show(response3.Content);

                        string text3 = response3.Content;
                        string[] words3 = text3.Split(delimiterChars);

                        string MensajeFinal, tipoDeDia = "dayType";
                        // if don´t exist a stop between the stop 1 and the last stop 7396 Es para ver si no hay alguna parada entre medias de 0 y la final
                        string AcceptPetition2 = words3[0];
                        if (AcceptPetition2 == "{")
                        {
                            tipoDeDia = words3[j];
                            while (tipoDeDia != "dayType")
                            {
                                j++;
                                tipoDeDia = words3[j];//153
                            }
                            j = j +2; //pos que dice si existe incidencia en esa linea
                            tipoDeDia = words3[j];

                            string url2 = "https://openapi.emtmadrid.es/v1/transport/busemtmad/lines/" + numLine + "/info/" + DateCorrect + "/";
                            var client4 = new RestClient(url2);
                            var request4 = new RestRequest(Method.GET);

                            request4.AddHeader("accessToken", numToken);
                            IRestResponse response4 = client4.Execute(request4);
                            string text4 = response4.Content;
                            string[] words4 = text4.Split(delimiterChars);
                            int BuscarEnLinea = 0;

                            while (words4[BuscarEnLinea] != tipoDeDia)
                            {
                                BuscarEnLinea++;//consigo sacar la pos y ahora me toca buscar hacia detras para mostrar los datos
                            }

                            while (words4[BuscarEnLinea] != "Direction1")
                            {
                                BuscarEnLinea--;
                            }
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine;

                            BuscarEnLinea = BuscarEnLinea + 12; //FrecuenciaText
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine;
                            BuscarEnLinea = BuscarEnLinea +4; //Start
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + " - ";
                            BuscarEnLinea = BuscarEnLinea + 4; //Finish
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine + Environment.NewLine;

                            while (words4[BuscarEnLinea] != "Direction2")
                            {
                                BuscarEnLinea--;
                            }
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine;
                            BuscarEnLinea = BuscarEnLinea + 12; //FrecuenciaText
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine;
                            BuscarEnLinea = BuscarEnLinea + 4; //Start
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + " - ";
                            BuscarEnLinea = BuscarEnLinea + 4; //Finish
                            MensajeFinalMostar = MensajeFinalMostar + words4[BuscarEnLinea] + Environment.NewLine + Environment.NewLine;

                            MensajeFinalMostar2 = MensajeFinalMostar2 + MensajeFinalMostar;
                            richTextBox1.Text = MensajeFinalMostar2;

                        }
                        else
                        {
                            richTextBox1.Text = "Date Incorrect";
                        }
                    }
                    else
                    {
                        //MessageBox.Show("The stop doesn´t exist");
                        richTextBox1.Text = "The BusLine doesn´t exist";
                    }

                    //            MessageBox.Show("correct");
                }
            }
            else
            {
                //MessageBox.Show("email  or password error");
                richTextBox1.Text = "email  or password error";
            }

        }
    }
}
