﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

using GMap.NET.MapProviders;
using GMap.NET;

namespace InterestPositionMap
{
    public partial class Form2 : Form
    {
        public string tokenrecibido, lat = "", longi  = "", rad = "";
        public string[] ArrayPuntosInteres;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //esta oculto el combobox y label de opciones de interes
            comboBox1.Hide();
            label5.Hide();
            gMapControl1.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.ResetText();
            validation1(tokenrecibido);
        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void validation1(string numToken)
        {


            string latitude = this.textBox1.Text;
            string longitude = this.textBox2.Text;
            string radius = this.textBox3.Text;
            //ejemplo de ejecucion
            /*longitude = "-3.7004000";
            latitude = "40.4146500";
            radius = "100";*/
            if (latitude == "" || longitude == "" || radius == "")
            {
                richTextBox1.Text = "WRONG DATA";
            }
            else
            {
                lat = latitude;
                longi = longitude;
                rad = radius;
                var client = new RestClient("https://openapi.emtmadrid.es/v1/citymad/places/arroundxy/ES/");
                var request = new RestRequest(Method.POST);
               
                request.AddHeader("accessToken", numToken);
                request.AddParameter("undefined", "{\"coordinates\":{\"longitude\":"+ longitude + ",\"latitude\":" + latitude + ",\"radius\":" + radius + "},\n\"family\":[{\"familyCode\":\"101\"}]}\n", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
                string text = response.Content;
                string[] words = text.Split(delimiterChars);
                string AcceptPetition = words[3];
                int numerodePoints = 0;
                if (AcceptPetition == "00")
                {
                    int i = 0;
                    while (words[i] != "description")
                    {
                        i++;
                    }
                    for(int j = 0; j < i; j++)
                    {
                        if(words[j] == "name")
                        {
                            numerodePoints++;//te dice el numero de puntos de interes que hay para generar un array para meterlos en el combobox
                        }
                    }
                    while (words[i] != "name")  //AQUI CONSIGO EL ULTIMO PUNTO DE INTERES
                    {
                        i--;//i tiene la ultima posicion de interes
                    }

                    ArrayPuntosInteres = new string[numerodePoints];
                    int contadorArray = 0;
                    for(int z = 0; z <= i; z++)
                    {
                        if (contadorArray < numerodePoints && words[z] == "name")
                        {
                            ArrayPuntosInteres[contadorArray] = words[z + 2];
                            comboBox1.Items.Add(ArrayPuntosInteres[contadorArray]);
                            contadorArray++; 
                        }
                    }
                    //muestro el combobox y label de opciones de interes
                    comboBox1.Show();
                    label5.Show();
                    gMapControl1.Show();
                }
                else
                {
                    richTextBox1.Text = "Petition Fail";
                }

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox1.SelectedItem;
            string puntoSelect = comboBox1.SelectedItem.ToString();

            MostrarDatos(lat, longi, rad, tokenrecibido, puntoSelect);
        }

        public void MostrarDatos(string latitude, string longitude, string radius, string numToken, string puntoSelect)
        {
            var client = new RestClient("https://openapi.emtmadrid.es/v1/citymad/places/arroundxy/ES/");
            var request = new RestRequest(Method.POST);

            request.AddHeader("accessToken", numToken);
            request.AddParameter("undefined", "{\"coordinates\":{\"longitude\":" + longitude + ",\"latitude\":" + latitude + ",\"radius\":" + radius + "},\n\"family\":[{\"familyCode\":\"101\"}]}\n", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            string AcceptPetition = words[3];
            string TextoMostrar = "";
            if (AcceptPetition == "00")
            {
                int i = 0;
                while(words[i] != puntoSelect)
                {
                    i++;//posicion de la seleccionada
                }
                TextoMostrar = TextoMostrar + words[i ] + Environment.NewLine;

                while(words[i] != "address")
                {
                    i--;
                }
                TextoMostrar = TextoMostrar +"Address: " +  words[i + 2] + Environment.NewLine;
                while (words[i] != "phone")
                {
                    i--;
                }
                while(words[i] != "coordinates")
                {
                    i--;
                }
                string CoordenadasSelect = words[i + 1];
                char[] delimiterCharsCoor = { '[', ',', ']' };
                string[] coordinArray = CoordenadasSelect.Split(delimiterCharsCoor);
                
           

                TextoMostrar = TextoMostrar + "Contact phone: " + words[i + 2] + Environment.NewLine;
                while (words[i] != "category")
                {
                    i--;
                }
                TextoMostrar = TextoMostrar + "Category: " + words[i + 2] + Environment.NewLine;

                richTextBox1.Text = TextoMostrar;//Muestra toda la informacion del seleccionado




                string coordinada1, coordinada2, coord11, coord12, coord13, coord22, coord23, coord24;
                //COORDENADAS A BUSCAR EN EL MAP DEL INTEREST POINT
                coordinada1 = coordinArray[1];
                coordinada2 = coordinArray[2];

                char[] delimiterCharsCoorPunto = { '.' };
                string[] coordinSep = coordinada1.Split(delimiterCharsCoorPunto);
                string[] coordinSep2 = coordinada2.Split(delimiterCharsCoorPunto);
                //valCoordenada1
                coord11 = coordinSep[0];
                coord12 = coordinSep[1];
                coord13 = coord12 + "]";


                int dividir = 0;
                long numFinal = 0;
                string division = "1";
                while (coord13[dividir] != ']')
                {
                    dividir++;
                    division = division + "0";
                }

                numFinal = Convert.ToInt64(division);


                double coordenada1Final, coordParse11 = Convert.ToDouble(coord11), coordParse12 = Convert.ToDouble(coordinSep[1]);
                coordParse12 = coordParse12 / numFinal;
                if (coordParse11 >= 0)
                {
                    coordenada1Final = coordParse11 + coordParse12;
                }
                else
                {
                    coordenada1Final = coordParse11 - coordParse12;
                }
                //valcoordenada2
                coord22 = coordinSep2[0];
                coord23 = coordinSep2[1];
                coord24 = coord23 + "]";


                int dividir2 = 0;
                long numFinal2 = 0;
                string division2 = "1";
                while (coord24[dividir2] != ']')
                {
                    dividir2++;
                    division2 = division2 + "0";
                }

                numFinal2 = Convert.ToInt64(division2);

                double coordenada2Final, coordParse22 = Convert.ToDouble(coord22), coordParse23 = Convert.ToDouble(coordinSep2[1]);


                coordParse23 = coordParse23 / numFinal2;
                if (coordParse22 >= 0)
                {
                    coordenada2Final = coordParse22 + coordParse23;
                }
                else
                {
                    coordenada2Final = coordParse22 - coordParse23;
                }

                //meter MAPA
                gMapControl1.DragButton = MouseButtons.Left;
                gMapControl1.CanDragMap = true;
                gMapControl1.MapProvider = GMapProviders.GoogleMap;
                gMapControl1.Position = new PointLatLng(coordenada2Final, coordenada1Final);
                gMapControl1.MinZoom = 0;
                gMapControl1.MaxZoom = 24;
                gMapControl1.Zoom = 18;
                gMapControl1.AutoScroll = true;


            }
        }
    }
}
