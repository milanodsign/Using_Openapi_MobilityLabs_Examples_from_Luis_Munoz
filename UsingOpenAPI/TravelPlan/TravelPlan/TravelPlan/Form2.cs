﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace TravelPlan
{
    public partial class Form2 : Form
    {
        public string tokenrecibido;

        public Form2()
        {
            InitializeComponent();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            string origen, destino, origenX, origenY, destinoX, destinoY;

            origen = this.textBox1.Text;
            origenY = this.textBox2.Text;
            origenX = this.textBox3.Text;
            destino = this.textBox4.Text;
            destinoY = this.textBox5.Text;
            destinoX = this.textBox6.Text;

            //Ejemplo de caso de uso
            origen = "Puentedeume";
            origenY = "40.47262";
            origenX = "-3.713674";
            destino = "finisterre";
            destinoY = "40.478858";
            destinoX = "-3.702354";

            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));

            String dy = datevalue.Day.ToString();
            if (DateTime.Now.Date.Day < 10)
            {
                dy = "0" + dy;
            }
            String mn = datevalue.Month.ToString();
            if (DateTime.Now.Date.Month < 10)
            {
                mn = "0" + mn;
            }
            String yy = datevalue.Year.ToString();
            String hour = datevalue.Hour.ToString();
            if (DateTime.Now.Date.Hour < 10)
            {
                hour = "0" + hour;
            }
            String min = datevalue.Minute.ToString();
            if (DateTime.Now.Date.Minute < 10)
            {
                min = "0" + min;
            }


            var client = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/travelplan/");
            var request = new RestRequest(Method.POST);

            request.AddHeader("accessToken", tokenrecibido);
            request.AddParameter("undefined", "{\n      \"cultureInfo\":\"ES\",\n      \"coordinateXFrom\":\"" + origenX + "\",\n      \"coordinateYFrom\":\"" + origenY + "\",\n      \"originName\":\"" + origen + "\",\n      \"coordinateXTo\":\"" + destinoX + "\",\n      \"coordinateYTo\":\"" + destinoY + "\",\n      \"destinationName\":\"" + destino + "\",\n      \"polygon\":\"\",\n      \"typeRoute\":\"30\",\n      \"detail\":\"Y\",\n      \"day\""+ dy + "\",\n      \"month\":\"" + mn + "\",\n      \"year\":\"" + yy + "\",\n      \"hour\":\"" + hour + "\",\n      \"minute\":\"" + min + "\"\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);

            string AcceptPetition, Mensaje = "";


            AcceptPetition = words[3];
            if (AcceptPetition == "00")
            {
                int i = 0, Ultimadireccion = 0;
                string valor, valorNuevo,  distancia;
                while(words[i] != "CodError")
                {
                    i++;
                }
                valor = words[i + 2];
                if (valor != "-1")
                {
                    while(words[i] != "Description")
                    {
                        i++;
                    }
                    while(words[i] != "Name")
                    {
                        i--;
                    }
                    Ultimadireccion = i;//tengo la ultima direccion 
                    Mensaje = "Ruta: " + origen + " - " + destino + Environment.NewLine;

                    int j = 0;
                    while(words[j] != "TimeToSpend")
                    {
                        j++;
                    }
                    valor = words[j + 2];
                    Mensaje = Mensaje + "Tiempo de duracion: " + valor + " Minutos" + Environment.NewLine + Environment.NewLine;
                    int contadorRuta = 0,contadorDistancia = 0;
                    while (words[contadorRuta] != "ListsRouteSection")
                    {
                        contadorRuta++;
                    }

                    while (contadorRuta <= Ultimadireccion)
                    {
                        

                        if(words[contadorRuta] == "Name")
                        {
                            valorNuevo = words[contadorRuta + 2];
                            if(valorNuevo != valor && valorNuevo != "OrderDetail")
                            {
                                valor = valorNuevo;
                                contadorDistancia = contadorRuta;
                                while(words[contadorDistancia] != "Distance")
                                {
                                    contadorDistancia--;
                                }
                                distancia = words[contadorDistancia + 2];
                                Mensaje = Mensaje + valor + "   " + Environment.NewLine;
                                Mensaje = Mensaje + distancia + Environment.NewLine;
                            }
                        }
                        contadorRuta++;
                    }


                }
                else
                {
                    Mensaje = "No se han podido obtener datos para la ruta solicitada";
                }

            }
            else
            {
                Mensaje = "Se ha producido un error";
            }

            richTextBox1.Text = Mensaje;
        }
    }
}