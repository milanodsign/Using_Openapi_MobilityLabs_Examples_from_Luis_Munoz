﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using RestSharp;

using GMap.NET.MapProviders;
using GMap.NET;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms;

namespace ArrivalTime
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation();
           
        }


        private void label3_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
                 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken;
            int  NumParadaMax, i = 0, j = 0, z = 41, zTime = z + 3;
            long numVal;//If the user introduces a number very long
            email = this.Txt1.Text;
            passwd = this.Txt2.Text;
            //numVal = this.Number1.Text;

            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);
            //If the user introduces a word(char) this show a error and if introduces a number(int) is correct  Esto es por si el usuario mete los valores a mano en un textbox
             try
             {
                 numVal = Int64.Parse(this.Number1.Text);
                
             }
             catch (FormatException )
             {
                 numVal = -1;
             }

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);


            if (response.Content[10] == compr[0])
            {
                
                numToken = words[19];
                var client2 = new RestClient("https://openapi.emtmadrid.es/v1/transport/busemtmad/stops/list/");
                var request2 = new RestRequest(Method.POST);

                request2.AddHeader("accessToken", numToken);
                IRestResponse response2 = client2.Execute(request2);


                string text2 = response2.Content;
                string[] words2 = text2.Split(delimiterChars);
                string AcceptPetition, ultimaParada;

                if( words2[0] != "<!DOCTYPE HTML PUBLIC ") { 
                AcceptPetition = words2[3];

                if (AcceptPetition == "00") {
                    //primera parada NumParada = words2[9]; para la siguiente parada son 24 pos masNumParada = words2[33];

                    ultimaParada = words2[i];

                    while (ultimaParada != "datetime")
                    {
                        i++;
                        ultimaParada = words2[i];
                    }
                    i = i - 26;// this is the position where we have the last stop number

                    //get the last stop that exists Obtengo la ultima parada que existe                    
                    ultimaParada = words2[i];
                    NumParadaMax = Int32.Parse(ultimaParada);

                    if (numVal > 0 && numVal <= NumParadaMax)
                    {
                        //              MessageBox.Show("parada existe");

                        //HERE work on requesting stop information
                        string url = "https://openapi.emtmadrid.es/v1/transport/busemtmad/stops/" + numVal + "/arrives/all/";

                        var client3 = new RestClient(url);
                        var request3 = new RestRequest(Method.POST);

                        request3.AddHeader("accessToken", numToken);
                        request3.AddParameter("undefined", "{\n      \"statistics\":\"N\",\n      \"cultureInfo\":\"EN\",\n      \"Text_StopRequired_YN\":\"Y\",\n      \"Text_EstimationsRequired_YN\":\"Y\",\n      \"Text_IncidencesRequired_YN\":\"Y\",\n      \"DateTime_Referenced_Incidencies_YYYYMMDD\":\"20180823\"\n}", ParameterType.RequestBody);
                        IRestResponse response3 = client3.Execute(request3);
                        //              MessageBox.Show(response3.Content);

                        string text3 = response3.Content;
                        string[] words3 = text3.Split(delimiterChars);
                        string NoBuses = ": [], ", direccionParada = "", miParada;

                        // if don´t exist a stop between the stop 1 and the last stop 7396 Es para ver si no hay alguna parada entre medias de 0 y la final
                        NoBuses = words3[6];
                        if (NoBuses == ": [], ") {
                            j = -1;
                        }

                        if (j == -1)
                        {
                            richTextBox1.Text = "The stop doesn´t exist";
                        }
                        else {


                            string numeroParadaParseado = numVal.ToString();
                            int posParadaJson = 0;
                            miParada = words2[posParadaJson];

                            while (miParada != numeroParadaParseado)
                            {
                                posParadaJson++;
                                miParada = words2[posParadaJson];
                            }
                            direccionParada = words2[posParadaJson + 9];

                            //Buscando las coordenadas de la parada
                            char[] delimiterCharsCoor = { '[', ',', ']' };
                            string[] coordinArray = direccionParada.Split(delimiterCharsCoor);
                            string coordinada1, coordinada2, coord11, coord12, coord13, coord22, coord23, coord24;
                            //Coordinadas para buscar en el mapa esa parada
                            coordinada1 = coordinArray[1];
                            coordinada2 = coordinArray[2];

                            char[] delimiterCharsCoorPunto = { '.' };
                            string[] coordinSep = coordinada1.Split(delimiterCharsCoorPunto);
                            string[] coordinSep2 = coordinada2.Split(delimiterCharsCoorPunto);
                            //valCoordenada1
                            coord11 = coordinSep[0];
                            coord12 = coordinSep[1];
                            coord13 = coord12 + "]";


                            int dividir = 0;
                            long numFinal = 0;
                            string division = "1";
                            while (coord13[dividir] != ']')
                            {
                                dividir++;
                                division = division + "0";
                            }

                            numFinal = Convert.ToInt64(division);


                            double coordenada1Final, coordParse11 = Convert.ToDouble(coord11), coordParse12 = Convert.ToDouble(coordinSep[1]);
                            coordParse12 = (coordParse12) / numFinal;
                            if (coordParse11 >= 0)
                            {
                                coordenada1Final = coordParse11 + coordParse12 ;
                            }
                            else
                            {
                                coordenada1Final = coordParse11 - coordParse12 ;
                            }
                            //valcoordenada2
                            coord22 = coordinSep2[0];
                            coord23 = coordinSep2[1];
                            coord24 = coord23 + "]";


                            int dividir2 = 0;
                            long numFinal2 = 0;
                            string division2 = "1";
                            while (coord24[dividir2] != ']')
                            {
                                dividir2++;
                                division2 = division2 + "0";
                            }

                            numFinal2 = Convert.ToInt64(division2);

                            double coordenada2Final, coordParse22 = Convert.ToDouble(coord22), coordParse23 = Convert.ToDouble(coordinSep2[1]);


                            coordParse23 = (coordParse23 ) / numFinal2;
                            if (coordParse22 >= 0)
                            {
                                coordenada2Final = coordParse22 + coordParse23;
                            }
                            else
                            {
                                coordenada2Final = coordParse22 - coordParse23;
                            }

                            int BuscName = posParadaJson + 9;
                            string BusName = words2[BuscName];

                            while (BusName != "name")
                            {
                                BuscName++;
                                BusName = words2[BuscName];
                            }
                            //Catch the Stop name
                            BusName = words2[BuscName + 2];

                            richTextBox1.Text = BusName + Environment.NewLine;
                            richTextBox1.Text = richTextBox1.Text + coordenada1Final + " , " + coordenada2Final;


                            GMapOverlay markersOverlay = new GMapOverlay("centro");
                            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(coordenada2Final , coordenada1Final ), GMarkerGoogleType.green);
                            gMapControl1.Overlays.Clear();
                            markersOverlay.Markers.Insert(0, marker);
                            gMapControl1.Overlays.Add(markersOverlay);
                            gMapControl1.DragButton = MouseButtons.Left;
                            gMapControl1.CanDragMap = true;
                            gMapControl1.MapProvider = GMapProviders.GoogleMap;
                            gMapControl1.Position = new PointLatLng(coordenada2Final , coordenada1Final );
                            gMapControl1.MinZoom = 0;
                            gMapControl1.MaxZoom = 24;
                            gMapControl1.Zoom = 18;
                            gMapControl1.AutoScroll = true;

                            

                            }
                    }
                    else
                    {
                        //MessageBox.Show("The stop doesn´t exist");
                        richTextBox1.Text = "The stop doesn´t exist";
                    }

                    //            MessageBox.Show("correct");
                }
            }
                else
                {
                    richTextBox1.Text = "fallo interno por fallo en url";
                }
             }
             else
             {
                //MessageBox.Show("email  or password error");
                richTextBox1.Text = "email  or password error";
            }
            
        }

        private void gMapControl1_Load(object sender, EventArgs e)
        {

        }
    }
    
}
