# Example Put Data In Metro Collection

_In this project I have created a example code that conects with MobilityLabs and checks the login.Then the program deletes all the data that exists using the DELETE method of the API_

### Requirements 📋

_we need use  Microsoft Visual Studio for c#_

_Use the framework .NET 4.6.1_

_we need add package RestSharp.106.4.0_

_We need the .xlsx with the Metro datas_

_we need internet connection for connect with the API_

_We can also use PostMan, because Postman helps us to work with the APIis_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)