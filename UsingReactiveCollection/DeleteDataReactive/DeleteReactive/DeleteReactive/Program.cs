﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Threading;
using System.Data.OleDb;
using System.Data;

namespace DeleteReactive
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string compr = "0S", numToken;


            string email, passwd;
            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();




            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {

                numToken = words[19];
                //Console.WriteLine("Email: " + email);
                //Console.WriteLine("Password: " + passwd);
                //Console.WriteLine(response.Content);
                //meter funcion que lee del excel
                BorrarDatos(numToken);


            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                //Console.WriteLine(response.Content);
            }

            Console.ReadKey();
        }

        private static void BorrarDatos(string numToken)
        {
            string cdCollection ="2D380836-7FA8-41CF-A4B5-BEB86AAD0090", cdLink = "1";
            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + cdCollection + "/" + cdLink + "/";
            var client = new RestClient(url);
            var request = new RestRequest(Method.DELETE);
            request.AddHeader("accessToken", numToken);
            request.AddParameter("undefined", "{}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine("Datos borrados");
        }

    }
}
