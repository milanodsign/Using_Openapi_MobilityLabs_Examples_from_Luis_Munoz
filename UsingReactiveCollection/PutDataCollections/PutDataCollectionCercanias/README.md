# Example Put Data In Cercanias Collection

_In this project I have created a example code that conects with MobilityLabs and checks the login.Then the program reads all the data of the Cercanias stops from an .xlsx, once read all the data it enters with a PUT method within the collection that had been created by me in MobilityLabs_

### Requirements 📋

_we need use  Microsoft Visual Studio for c#_

_Use the framework .NET 4.6.1_

_we need add package RestSharp.106.4.0_

_We need the .xlsx with the Cercanias datas_

_we need internet connection for connect with the API_

_We can also use PostMan, because Postman helps us to work with the APIis_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)