﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Threading;
using System.Data.OleDb;
using System.Data;
namespace PutDataCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string compr = "0S", numToken;


            string email, passwd;
            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();

            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {

                numToken = words[19];
                //Console.WriteLine("Email: " + email);
                //Console.WriteLine("Password: " + passwd);
                //Console.WriteLine(response.Content);
                //meter funcion que lee del excel
                leerExcel(numToken);


            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                //Console.WriteLine(response.Content);
            }

            Console.ReadKey();


        }

        public static void leerExcel(string numtoken)
        {
            int numeroFilasFichero = 19;
            string strPath = @"E:\Practicas\PracticasEMT\ExamplesMobilityLabs\UsingReactiveCollection\PutDataCollections\PutDataCollectionCercanias\ParadasCercanias.xlsx";
            string cdCollection = "9869B1DA-F3E0-4FD2-A615-2234702BD272", cdLink = "1";
            string descripcion = "Documento%20con%20las%20paradas%20de%20Cercanias";
            string[] datos = new string[numeroFilasFichero];
            var conn = new OleDbConnection();
            var cmd = new OleDbCommand();
            var da = new OleDbDataAdapter();
            var ds = new DataSet();
            try
            {
                conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Mode=Read;Extended Properties=Excel 8.0;Persist Security Info=False;";
                cmd.CommandText = "SELECT * FROM [Hoja1$]"; // no olivdar incluir el simbolo de peso $
                cmd.Connection = conn;
                da.SelectCommand = cmd;
                conn.Open();
                da.Fill(ds);
                foreach (DataRow fila in ds.Tables[0].Rows)
                {
                    for (int i = 0; i < numeroFilasFichero; i++)
                    {
                        datos[i] = fila[i].ToString();
                        //Console.Write(fila[i] + "  ");
                        //Console.Write(datos[i] + "  ");
                    }

                    if (datos[1] == "CERCANIAS.Station")
                    {
                        string idKey = datos[0];
                        string direccionCompleta = datos[10] + " " + datos[11] + " " + datos[12];
                        //meto los valores de cada una de las filas en la coleccion
                        var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + cdCollection + "/" + cdLink + "/" + idKey + "/fa-train/blue/red/31536000/PUBLIC/" + descripcion + "/");
                        var request = new RestRequest(Method.PUT);

                        request.AddHeader("accessToken", numtoken);

                        string lat = "", longi = "";
                        char[] delimiterChars = { ',' };
                        string text = datos[16];
                        string[] words = text.Split(delimiterChars);
                        lat = words[0] + "." + words[1];
                        text = datos[15];
                        words = text.Split(delimiterChars);
                        longi = words[0] + "." + words[1];

                        for (int j = 0; j < numeroFilasFichero; j++)
                        {
                            datos[j] = datos[j] + "\"";
                        }

                        string lineas = datos[18] + ",.";
                       
                        string TratarLineas = "";
                        char[] delimiterChars2 = { ',', ' ' };
                        string[] words3 = lineas.Split(delimiterChars2);
                        int i = 0;
                        bool nada = false;
                        while (words3[i] != ".")
                        {
                            if (words3[i] != "" && words3[i] != "\"")
                            {
                                TratarLineas = TratarLineas + "\"" + words3[i] + "\"" + ",\r\n\t\t\t";
                                nada = true;

                            }
                            i++;
                        }
                        if(nada)
                            lineas = TratarLineas.Substring(0, TratarLineas.Length - 7);
                        else
                        {
                            lineas = "";                   
                        }


                        Console.WriteLine("{ \r\n\t\"idStop\":\"" + datos[0] + " ,\r\n\t\t\"mode\":\"" + datos[1] + ",\r\n\t\t\"stopCode\":\"" + datos[2] + ",\r\n\t\t\"name\":\"" + datos[3] + ",\r\n\t\t\"CTMEstacionRedMetroCode\":\"" + datos[4] + ",\r\n\t\t\"companyCode\":\"" + datos[5] + ",\r\n\t\t\"provinceCode\":\"" + datos[6] + ",\r\n\t\t\"municipalityCode\":\"" + datos[7] + ",\r\n\t\t\"entityCode\":\"" + datos[8] + ",\r\n\t\t\"coreCode\":\"" + datos[9] + ",\r\n\t\t\"address\":\"" + direccionCompleta + ", " + datos[13] + ",\r\n\t\t\"postalCode\":\"" + datos[14] + ",\r\n\t\t\"geometry\": {\r\n\t\t\t\"type\": \"Point\",\r\n\t\t\t\"coordinates\": [\r\n\t\t\t\t" + longi + ",\r\n\t\t\t\t" + lat + "\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"Accesibility\":\"" + datos[17] + ",\r\n\t\t\"cercaniasLines\":[\r\n\t\t\t" + lineas + "\r\n\t\t]                       \r\n}");
                        
                        request.AddParameter("", "{ \r\n\t\"idStop\":\"" + datos[0] + " ,\r\n\t\t\"mode\":\"" + datos[1] + ",\r\n\t\t\"stopCode\":\"" + datos[2] + ",\r\n\t\t\"name\":\"" + datos[3] + ",\r\n\t\t\"CTMEstacionRedMetroCode\":\"" + datos[4] + ",\r\n\t\t\"companyCode\":\"" + datos[5] + ",\r\n\t\t\"provinceCode\":\"" + datos[6] + ",\r\n\t\t\"municipalityCode\":\"" + datos[7] + ",\r\n\t\t\"entityCode\":\"" + datos[8] + ",\r\n\t\t\"coreCode\":\"" + datos[9] + ",\r\n\t\t\"address\":\"" + direccionCompleta + ", " + datos[13] + ",\r\n\t\t\"postalCode\":\"" + datos[14] + ",\r\n\t\t\"geometry\": {\r\n\t\t\t\"type\": \"Point\",\r\n\t\t\t\"coordinates\": [\r\n\t\t\t\t" + longi + ",\r\n\t\t\t\t" + lat + "\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"Accesibility\":\"" + datos[17] + ",\r\n\t\t\"cercaniasLines\":[\r\n\t\t\t" + lineas + "\r\n\t\t]                       \r\n}", ParameterType.RequestBody);
                        
                        IRestResponse response = client.Execute(request);
                        string contenido = response.Content;
                        string[] words2 = contenido.Split(delimiterChars);
                        if (words2[0] == "{\"code\": \"90\"")
                        {
                            Console.WriteLine("ERRORRRRRRRRRRR");
                        }
                    }

                    //Console.Write( fila[0]);
                }
                Console.Write("Fichero Procesado Correctamente");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }
}
