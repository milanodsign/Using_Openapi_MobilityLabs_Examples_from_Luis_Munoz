﻿using System;
using RestSharp;
using System.Threading;
using System.Data.OleDb;
using System.Data;
namespace PutDataCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string compr = "0S", numToken;


            string email, passwd;
            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();
            //Connect with API





            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password",passwd);
            request.AddHeader("email", email);

            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {

                numToken = words[19];
                //Console.WriteLine("Email: " + email);
                //Console.WriteLine("Password: " + passwd);
                //Console.WriteLine(response.Content);
                //meter funcion que lee del excel
                leerExcel(numToken);


            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                //Console.WriteLine(response.Content);
            }

            Console.ReadKey();

        }

        public static void leerExcel(string numtoken)
        {
            int numeroFilasFichero = 17;
            string strPath = @"E:\Practicas\PracticasEMT\ExamplesMobilityLabs\UsingReactiveCollection\PutDataCollections\PutDataCollectionInterUrbanosPrivate\ParadasInterUrbanos.xlsx";
            string cdCollection = "A7C6203F-4887-4A00-9D8E-AF15D162ED19", cdLink = "1";
            string descripcion = "Documento%20con%20las%20paradas%20de%20InterUrbanos";
            string[] datos = new string[numeroFilasFichero];
            var conn = new OleDbConnection();
            var cmd = new OleDbCommand();
            var da = new OleDbDataAdapter();
            var ds = new DataSet();
            try
            {
                conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Mode=Read;Extended Properties=Excel 8.0;Persist Security Info=False;";
                cmd.CommandText = "SELECT * FROM [Hoja1$]"; // no olivdar incluir el simbolo de peso $
                cmd.Connection = conn;
                da.SelectCommand = cmd;
                conn.Open();
                da.Fill(ds);
                foreach (DataRow fila in ds.Tables[0].Rows)
                {
                    for (int s = 0; s < numeroFilasFichero; s++)
                    {
                        datos[s] = fila[s].ToString();
                        //Console.Write(fila[i] + "  ");
                        //Console.Write(datos[i] + "  ");
                    }

                    string idKey = datos[0];
                   
                    //meto los valores de cada una de las filas en la coleccion
                    var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + cdCollection + "/" + cdLink + "/" + idKey + "/fa-car/blue/red/31536000/PUBLIC/" + descripcion + "/");
                    var request = new RestRequest(Method.PUT);

                    request.AddHeader("accessToken", numtoken);

                    string lat = "", longi = "";
                    char[] delimiterChars = { ',' };
                    string text = datos[11];
                    string[] words = text.Split(delimiterChars);
                    lat = words[0] + "." + words[1];
                    text = datos[12];
                    words = text.Split(delimiterChars);
                    longi = words[0] + "." + words[1];
                    
                    Console.WriteLine("{ \r\n\t\"idStop\":\"" + datos[0] + "\",\r\n\t\"idCompany\":\"" + datos[1] + "\",\r\n\t\"idActor\":\"" + datos[2] + "\",\r\n\t\"concession\":\"" + datos[3] + "\",\r\n\t\"idLineGestra\":\"" + datos[4] + "\",\r\n\t\"DLine\":\"" + datos[5] + "\",\r\n\t\"idStopgestra\":\"" + datos[6] + "\",\r\n\t\"stopName\":\"" + datos[7] + "\",\r\n\t\"municipalityCode\":\"" + datos[8]  + "\",\r\n\t\"postalCode\":\"" + datos[9] + "\",\r\n\t\"rateZone\":\"" + datos[10] + "\",\r\n\t\"geometry\": {\r\n\t\t\"type\": \"Point\",\r\n\t\t\"coordinates\": [\r\n\t\t\t" + longi + ",\r\n\t\t\t" + lat + "\r\n\t\t]\r\n\t},\r\n\t\"zoning\":\"" + datos[13] + "\",\r\n\t\"idLine\":\"" + datos[14] + "\",\r\n\t\"Dpaypoint\":\"" + datos[15] + "\",\r\n\t\"idTopology\":\"" + datos[16] + "\"\r\n\t\t                       \r\n}");

                    request.AddParameter("", "{ \r\n\t\"idStop\":\"" + datos[0] + "\",\r\n\t\"idCompany\":\"" + datos[1] + "\",\r\n\t\"idActor\":\"" + datos[2] + "\",\r\n\t\"concession\":\"" + datos[3] + "\",\r\n\t\"idLineGestra\":\"" + datos[4] + "\",\r\n\t\"DLine\":\"" + datos[5] + "\",\r\n\t\"idStopgestra\":\"" + datos[6] + "\",\r\n\t\"stopName\":\"" + datos[7] + "\",\r\n\t\"municipalityCode\":\"" + datos[8] + "\",\r\n\t\"postalCode\":\"" + datos[9] + "\",\r\n\t\"rateZone\":\"" + datos[10] + "\",\r\n\t\"geometry\": {\r\n\t\t\"type\": \"Point\",\r\n\t\t\"coordinates\": [\r\n\t\t\t" + longi + ",\r\n\t\t\t" + lat + "\r\n\t\t]\r\n\t},\r\n\t\"zoning\":\"" + datos[13] + "\",\r\n\t\"idLine\":\"" + datos[14] + "\",\r\n\t\"Dpaypoint\":\"" + datos[15] + "\",\r\n\t\"idTopology\":\"" + datos[16] + "\"\r\n\t\t                       \r\n}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                   
                }
               
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }
}