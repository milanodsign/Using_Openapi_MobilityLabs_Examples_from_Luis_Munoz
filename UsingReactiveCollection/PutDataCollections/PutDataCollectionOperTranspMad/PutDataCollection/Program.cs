﻿using System;
using RestSharp;
using System.Threading;
using System.Data.OleDb;
using System.Data;
namespace PutDataCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string compr = "0S", numToken;


            string email , passwd ;
            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();
            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);


            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {

                numToken = words[19];
                //Console.WriteLine("Email: " + email);
                //Console.WriteLine("Password: " + passwd);
                //Console.WriteLine(response.Content);
                //meter funcion que lee del excel
                leerExcel(numToken);


            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                //Console.WriteLine(response.Content);
            }

            Console.ReadKey();

        }

        public static void leerExcel(string numtoken)
        {
            int numeroFilasFichero = 6;
            string strPath = @"E:\Practicas\PracticasEMT\ExamplesMobilityLabs\UsingReactiveCollection\PutDataCollections\PutDataCollectionOperTranspMad\OperadoresTransMad2.xlsx";
            string cdCollection = "BBF93681-85EB-4DD5-A8A6-2C0E9BC1B0C1", cdLink = "1";
            string descripcion = "Documento%20con%20los%20operadores%20de%20transporte%20de%20madrid";
            string[] datos = new string[numeroFilasFichero];
            var conn = new OleDbConnection();
            var cmd = new OleDbCommand();
            var da = new OleDbDataAdapter();
            var ds = new DataSet();
            try
            {
                conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Mode=Read;Extended Properties=Excel 8.0;Persist Security Info=False;";
                cmd.CommandText = "SELECT * FROM [Hoja1$]"; // no olivdar incluir el simbolo de peso $
                cmd.Connection = conn;
                da.SelectCommand = cmd;
                conn.Open();
                da.Fill(ds);
                foreach (DataRow fila in ds.Tables[0].Rows)
                {
                    for (int i = 0; i < numeroFilasFichero; i++)
                    {
                        datos[i] = fila[i].ToString();
                        //Console.Write(fila[i] + "  ");
                        //Console.Write(datos[i] + "  ");
                    }

                    string idKey = datos[0];
                    //meto los valores de cada una de las filas en la coleccion
                    var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + cdCollection + "/" + cdLink + "/" + idKey + "/fa-car/blue/red/31536000/PUBLIC/" + descripcion + "/");
                    var request = new RestRequest(Method.PUT);

                    request.AddHeader("accessToken", numtoken);

                    Console.WriteLine("{ \r\n\t\"idCompany\":\"" + datos[0] + "\" ,\r\n\t\"idActor\":\"" + datos[1] + "\",\r\n\t\"btOperator\":\"" + datos[2] + "\",\r\n\t\"Nsempre\":\"" + datos[3] + "\",\r\n\t\"companyName\":\"" + datos[4] + "\"                       \r\n}");
                  
                    request.AddParameter("", "{ \r\n\t\"idCompany\":\"" + datos[0] + "\" ,\r\n\t\"idActor\":\"" + datos[1] + "\",\r\n\t\"btOperator\":\"" + datos[2] + "\",\r\n\t\"Nsempre\":\"" + datos[3] + "\",\r\n\t\"companyName\":\"" + datos[4] + "\"                       \r\n}", ParameterType.RequestBody);
                    
                    IRestResponse response = client.Execute(request);

                 
                }
                Console.Write("Fichero Procesado Correctamente");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
    }
}