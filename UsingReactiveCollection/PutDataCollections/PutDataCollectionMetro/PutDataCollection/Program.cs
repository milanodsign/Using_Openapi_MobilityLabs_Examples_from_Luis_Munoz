﻿using System;
using System.Linq;
using RestSharp;
using System.Threading;
using System.Data.OleDb;
using System.Data;
namespace PutDataCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string compr = "0S", numToken;


            string email , passwd ;
            Console.Write("email: ");
            email = Console.ReadLine();
            Console.Write("password: ");
            passwd = Console.ReadLine();
            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);


            Console.Write("Waiting connection");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.Write(".");
            Thread.Sleep(350);
            Console.WriteLine(".");
            IRestResponse response = client.Execute(request);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);
            //verification in the response obtained that the data has been correct

            if (response.Content[10] == compr[0])
            {

                numToken = words[19];
                //Console.WriteLine("Email: " + email);
                //Console.WriteLine("Password: " + passwd);
                //Console.WriteLine(response.Content);
                //meter funcion que lee del excel
                leerExcel(numToken);


            }
            else
            {
                Console.WriteLine("Error in the  Email: " + email + " or password");
                //Console.WriteLine(response.Content);
            }

            Console.ReadKey();

        }

        public static void leerExcel(string numtoken)
        {
            int numeroFilasFichero = 19;
            string strPath = @"E:\Practicas\PracticasEMT\ExamplesMobilityLabs\UsingReactiveCollection\PutDataCollections\PutDataCollectionMetro\ParadasMetro2.xlsx";
            string cdCollection = "60049BBD-FB48-458A-BC01-416BE637AEB3", cdLink = "1";
            string descripcion = "Documento%20con%20las%20paradas%20de%20Metro";
            string[] datos = new string[numeroFilasFichero];
            var conn = new OleDbConnection();
            var cmd = new OleDbCommand();
            var da = new OleDbDataAdapter();
            var ds = new DataSet();
            try
            {
                conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath + ";Mode=Read;Extended Properties=Excel 8.0;Persist Security Info=False;";
                cmd.CommandText = "SELECT * FROM [Hoja1$]"; // no olivdar incluir el simbolo de peso $
                cmd.Connection = conn;
                da.SelectCommand = cmd;
                conn.Open();
                da.Fill(ds);
                foreach (DataRow fila in ds.Tables[0].Rows)
                {
                    for (int i = 0; i < numeroFilasFichero; i++)
                    {
                        datos[i] = fila[i].ToString();
                        //Console.Write(fila[i] + "  ");
                        //Console.Write(datos[i] + "  ");
                    }

                    if (datos[1] == "METROMAD.Station")//esto es solo para guardar las de meytro que son con Modo = 4
                    {
                        string idKey = datos[0];
                        string direccionCompleta = datos[10] + " " + datos[11] + " " + datos[12];
                        //meto los valores de cada una de las filas en la coleccion
                        var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/collection/reactive/" + cdCollection + "/" + cdLink + "/" + idKey + "/fa-train/blue/red/31536000/PUBLIC/" + descripcion + "/");
                        var request = new RestRequest(Method.PUT);

                        request.AddHeader("accessToken", numtoken);

                        //DEBO TRANSFORMAR LAS COORDENADAS
                        /*double Y = Convert.ToDouble(datos[16]);
                        double X = Convert.ToDouble(datos[15]);
                        double latitude = 0, longitude = 0;
                        string utmZone = "30T";//esta es la UTM zone de Madrid

                        ToLatLon(X, Y,utmZone, out latitude,out longitude);
                        string lat = "", longi = "";
                        char[] delimiterChars = {',' };
                        string text = latitude.ToString();
                        string[] words = text.Split(delimiterChars);
                        lat = words[0] + "." + words[1];
                        text = longitude.ToString();
                        words = text.Split(delimiterChars);
                        longi = words[0] + "." + words[1];
                        for(int j = 0; j < numeroFilasFichero; j++)
                        {
                            datos[j] = datos[j] + "\"";
                        }*/
                        
                        string lat = "", longi = "";
                        char[] delimiterChars = { ',' };
                        string text = datos[16];
                        string[] words = text.Split(delimiterChars);
                        lat = words[0] + "." + words[1];
                        text = datos[15];
                        words = text.Split(delimiterChars);
                        longi = words[0] + "." + words[1];

                        for (int j = 0; j < numeroFilasFichero; j++)
                        {
                            datos[j] = datos[j] + "\"";
                        }

                        string lineas = datos[18] + ",.";

                        string TratarLineas = "";
                        char[] delimiterChars2 = { ',', ' ' };
                        string[] words3 = lineas.Split(delimiterChars2);
                        int i = 0;
                        bool nada = false;
                        while (words3[i] != ".")
                        {
                            if (words3[i] != "" && words3[i] != "\"")
                            {
                                TratarLineas = TratarLineas + "\""  + words3[i] + "\"" + ",\r\n\t\t\t";
                                nada = true;

                            }
                            i++;
                        }
                        if (nada)
                            lineas = TratarLineas.Substring(0, TratarLineas.Length - 7);
                        else
                        {
                            lineas = "";
                        }

                        Console.WriteLine("{ \r\n\t\"idStop\":\"" + datos[0] + " ,\r\n\t\t\"mode\":\"" + datos[1] + ",\r\n\t\t\"stopCode\":\"" + datos[2] + ",\r\n\t\t\"name\":\"" + datos[3] + ",\r\n\t\t\"CTMEstacionRedMetroCode\":\"" + datos[4] + ",\r\n\t\t\"companyCode\":\"" + datos[5] + ",\r\n\t\t\"provinceCode\":\"" + datos[6] + ",\r\n\t\t\"municipalityCode\":\"" + datos[7] + ",\r\n\t\t\"entityCode\":\"" + datos[8] + ",\r\n\t\t\"coreCode\":\"" + datos[9] + ",\r\n\t\t\"address\":\"" + direccionCompleta + ", " + datos[13] + ",\r\n\t\t\"postalCode\":\"" + datos[14] + ",\r\n\t\t\"geometry\": {\r\n\t\t\t\"type\": \"Point\",\r\n\t\t\t\"coordinates\": [\r\n\t\t\t\t" + longi + ",\r\n\t\t\t\t" + lat + "\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"Accesibility\":\"" + datos[17] + ",\r\n\t\t\"metroLines\":[\r\n\t\t\t" + lineas + "\r\n\t\t]                       \r\n}");
                        //Console.WriteLine("{ \n\t\t\"idEstacion\":\"" + datos[0] + ",\n\t\t{\n\t\t\t\"modo\":\"" + datos[1] + "\",\n\t\t\t\"codigoEstacion\":\"" + datos[2] + "\",\n\t\t\t\"denominacion\":\"" + datos[3] + "\",\n\t\t\t\"codigoCTMEstacionRedMetro\":\"" + datos[4] + "\",\n\t\t\t\"codigoEmpresa\":\"" + datos[5] + "\",\n\t\t\t\"codigoProvincia\":\"" + datos[6] + "\",\n\t\t\t\"codigoMunicipio\":\"" + datos[7] + "\",\n\t\t\t\"codigoEntidad\":\"" + datos[8] + "\",\n\t\t\t\"codigoNucleo\":\"" + datos[9] + "\",\n\t\t\t\"via\":\"" + direccionCompleta + ", " + datos[13] + " \",\n\t\t\t\"codigoPostal\":\"" + datos[14] + "\",\n            \"geometry\": {\n                \"type\": \"Point\",\n                \"coordinates\": [\"\n                    " + longitude + "\n                    " + latitude + "\"\n                ]},\n            \"gradoAccesibilidad\":\"" + datos[17] + "\",\n               \n        }\n  }");
                        request.AddParameter("", "{ \r\n\t\"idStop\":\"" + datos[0] + " ,\r\n\t\t\"mode\":\"" + datos[1] + ",\r\n\t\t\"stopCode\":\"" + datos[2] + ",\r\n\t\t\"name\":\"" + datos[3] + ",\r\n\t\t\"CTMEstacionRedMetroCode\":\"" + datos[4] + ",\r\n\t\t\"companyCode\":\"" + datos[5] + ",\r\n\t\t\"provinceCode\":\"" + datos[6] + ",\r\n\t\t\"municipalityCode\":\"" + datos[7] + ",\r\n\t\t\"entityCode\":\"" + datos[8] + ",\r\n\t\t\"coreCode\":\"" + datos[9] + ",\r\n\t\t\"address\":\"" + direccionCompleta + ", " + datos[13] + ",\r\n\t\t\"postalCode\":\"" + datos[14] + ",\r\n\t\t\"geometry\": {\r\n\t\t\t\"type\": \"Point\",\r\n\t\t\t\"coordinates\": [\r\n\t\t\t\t" + longi + ",\r\n\t\t\t\t" + lat + "\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"Accesibility\":\"" + datos[17] + ",\r\n\t\t\"metroLines\":[\r\n\t\t\t" + lineas + "\r\n\t\t]                       \r\n}", ParameterType.RequestBody);
                        //request.AddParameter("", "{ \n\t\t\"idEstacion\":\"" + datos[0] + ",\n\t\t{\n\t\t\t\"modo\":\"" + datos[1] + "\",\n\t\t\t\"codigoEstacion\":\"" + datos[2] + "\",\n\t\t\t\"denominacion\":\"" + datos[3] + "\",\n\t\t\t\"codigoCTMEstacionRedMetro\":\"" + datos[4] + "\",\n\t\t\t\"codigoEmpresa\":\"" + datos[5] + "\",\n\t\t\t\"codigoProvincia\":\"" + datos[6] + "\",\n\t\t\t\"codigoMunicipio\":\"" + datos[7] + "\",\n\t\t\t\"codigoEntidad\":\"" + datos[8] + "\",\n\t\t\t\"codigoNucleo\":\"" + datos[9] + "\",\n\t\t\t\"via\":\"" + direccionCompleta + ", " + datos[13] + " \",\n\t\t\t\"codigoPostal\":\"" + datos[14] + "\",\n            \"geometry\": {\n                \"type\": \"Point\",\n                \"coordinates\": [\"\n                    " + longitude + "\n                    " + latitude + "\"\n                ]},\n            \"gradoAccesibilidad\":\"" + datos[17] + "\",\n               \n        }\n  }", ParameterType.RequestBody);
                        IRestResponse response = client.Execute(request);
                    }

                    //Console.Write( fila[0]);
                }
                Console.Write("Fichero Procesado Correctamente");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        //Cuando el excel no tenia latitud y longitud y habia que transformarlas
        public static void ToLatLon(double utmX, double utmY, string utmZone, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = utmZone.Last() >= 'N';

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = int.Parse(utmZone.Remove(utmZone.Length - 1));
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }
    }
}