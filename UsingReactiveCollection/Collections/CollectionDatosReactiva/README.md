# Example Collections

_In this project, I created an example code that connects to MobilityLabs and verifies the login. Then, the user can select only one reactive category and the program shows all the reactive subcategories, the user selects a subcategory, later shows the reactive collections, the user needs to select a collections, In addition, we can put a query for the collection previously selected. Finally the program shows details of the selected collection_

### Requirements 📋

_we need use  Microsoft Visual Studio for c#_

_Use the framework .NET 4.6.1_

_we need add package RestSharp.106.4.0_

_we need internet connection for connect with the API_

_We can also use PostMan, because Postman helps us to work with the APIis_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)