﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace Collection
{
    public partial class Form1 : Form
    {
        //public IContract TokenVuelta { get; set; }
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string token = Validation();
            if (token != "")
            {
                //Application.Run(new Form2(token));
                //TokenVuelta.Ejecutar(token);
                Form2 coleccion = new Form2();
                coleccion.tokenrecibido = token;
                coleccion.Show();
                this.Hide();

            }

            else {
                this.textBox1.Text = "";
                this.textBox2.Text = "";

            }
        }

        public string Validation()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            string email, passwd, compr = "0S", numToken = "";

            email = this.textBox1.Text;
            passwd = this.textBox2.Text;
            


            //Connect with API
            var client = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/user/login/");
            var request = new RestRequest(Method.GET);

            request.AddHeader("password", passwd);
            request.AddHeader("email", email);

            IRestResponse response = client.Execute(request);


            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text = response.Content;
            string[] words = text.Split(delimiterChars);


            if (response.Content[10] == compr[0])
            {
                //List all LineBuses
                numToken = words[19];
            }
            return numToken;
        }
    }
}
