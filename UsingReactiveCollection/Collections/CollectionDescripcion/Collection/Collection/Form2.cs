﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;

namespace Collection
{
    public partial class Form2 : Form
    {
        public string tokenrecibido, num2Seleccionado;

        public int numeroDeCategorias = 0, numeroDeColecciones = 0;
        public int[] ContadorArray;
        public string[] Colecciones, CategoriaNombre, CategoriaNumero, SubCategoriaNombre, SubCategoriaNumero;
        public string NombreColeccion;
        int numeroS;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            InicializoCategoria(tokenrecibido);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox1.SelectedItem;
            string catSeleccionada = comboBox1.SelectedItem.ToString();
            //MessageBox.Show(comboBox1.SelectedText);
            //MessageBox.Show(comboBox1.SelectedText);
            
            MuestroSubcategorias(catSeleccionada);
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            object num = comboBox2.SelectedItem;
            string subcatSeleccionada = comboBox2.SelectedItem.ToString();

            //MessageBox.Show(comboBox2.SelectedText);
            //cob2Seleccionado = comboBox2.SelectedText;
            MuestroColeccion(tokenrecibido, subcatSeleccionada);
            //MuestroColeccionCorrecta(tokenrecibido);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

            object num = comboBox3.SelectedItem;
            string colSeleccionada = comboBox3.SelectedItem.ToString();
            string numeroSub = colSeleccionada;

            //MessageBox.Show(comboBox3.SelectedText);
            
            //string numeroSub = comboBox3.SelectedText;

            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + numeroS + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenrecibido);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            int lastCol = 0;
            string ultimaCol = "";
            while (ultimaCol != numeroSub)
            {
                lastCol++;
                ultimaCol = words2[lastCol];
            }

            NombreColeccion = words2[lastCol - 12 ];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Validation(tokenrecibido);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void InicializoCategoria(string tokenr)
        {
            var client2 = new RestClient("https://openapi.emtmadrid.es/v1/mobilitylabs/discover/categories/");
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            int lastCat = 0;
            string ultimaCat;
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            string AcceptPetition;

            

            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimaCat = words2[lastCat];
                while (ultimaCat != "description")
                {
                    lastCat++;
                    ultimaCat = words2[lastCat];
                }
                lastCat = lastCat - 36; //this is the position where we have the last categoria

                ultimaCat = words2[lastCat];

                int posCat = 0;

                //ultimaCat = words2[posCat];
                while (posCat <= lastCat)
                {
                    if (ultimaCat == "DS_CATEGORY")
                    {
                        numeroDeCategorias++;
                    }
                    posCat++;
                    ultimaCat = words2[posCat];
                }
                posCat = 0;
                int meteDatos = 0,contArra = 0;
                ContadorArray = new int[numeroDeCategorias];
                CategoriaNombre = new string[numeroDeCategorias];
                CategoriaNumero = new string[numeroDeCategorias];
                SubCategoriaNombre = new string[numeroDeCategorias];
                SubCategoriaNumero = new string[numeroDeCategorias];
                for(int j = 0; j < numeroDeCategorias; j++)
                {
                    ContadorArray[j] = contArra;
                }
                string GuardarNumero = "";
                char[] delimiterChars2 = {',', ':' };
                string[] words3, words4;
                ultimaCat = words2[posCat];
                while (posCat <= lastCat)
                {
                    if (ultimaCat == "DS_CATEGORY")
                    {
                        CategoriaNombre[meteDatos] = words2[posCat + 2];
                        SubCategoriaNombre[meteDatos] = words2[posCat + 8];

                        GuardarNumero = words2[posCat + 11];
                        words3 = GuardarNumero.Split(delimiterChars2);
                        CategoriaNumero[meteDatos] = words3[1];

                        GuardarNumero = words2[posCat + 33];
                        words4 = GuardarNumero.Split(delimiterChars2);
                        SubCategoriaNumero[meteDatos] = words4[1];

                        for (int i = 0; i < numeroDeCategorias; i++)
                        {
                            if(CategoriaNombre[meteDatos] == CategoriaNombre[i])
                            {
                                contArra = ContadorArray[i];
                                contArra++;
                                ContadorArray[i] = contArra;//este contador tiene las posiciones con categorias diferentes
                            }
                        }
                        meteDatos++;
                    }
                    posCat++;
                    ultimaCat = words2[posCat];
                }

               int z = 0;
                while(ContadorArray[z] > 0){
                    comboBox1.Items.Add(CategoriaNombre[z]);
                    z++;
                }
            }
        }

        public void MuestroSubcategorias(string catSel)
        {
            //comboBox1.SelectedText = comboBox1.SelectedText;
            int buscoCategoriaSeleccionada = 0, numeroSubcategorias = 0;
            while (CategoriaNombre[buscoCategoriaSeleccionada] != catSel)
            {
                buscoCategoriaSeleccionada++;
            }
            numeroSubcategorias = ContadorArray[buscoCategoriaSeleccionada];

            for(int contadorSub = 0; contadorSub < numeroSubcategorias; contadorSub++)
            {
                for(int i = 0; i < numeroDeCategorias; i++)
                {
                    if(catSel == CategoriaNombre[i])
                    {
                        comboBox2.Items.Add(SubCategoriaNombre[i]);
                    }
                }
            }

        }

        public void MuestroColeccion(string tokenr, string subcatSel)
        {
            //MessageBox.Show(comboBox2.SelectedText);
            int buscoCategoriaSeleccionada = 0;
            while (SubCategoriaNombre[buscoCategoriaSeleccionada] != subcatSel)
            {
                buscoCategoriaSeleccionada++;
            }
            num2Seleccionado = SubCategoriaNumero[buscoCategoriaSeleccionada];
            string numeroSub = SubCategoriaNumero[buscoCategoriaSeleccionada];
            numeroS = Int32.Parse(numeroSub);
        
            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + numeroS + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);

            string AcceptPetition;
            int lastCol = 0;
            string ultimaCol;
            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimaCol = words2[lastCol];
                while (ultimaCol != "description")
                {
                    lastCol++;
                    ultimaCol = words2[lastCol];
                }
                lastCol = lastCol - 14; //this is the position where we have the last coleccion
                //ultimaCol = words2[lastCol];

                int posCol = 0;
                while (posCol <= lastCol)
                {
                    if (ultimaCol == "DS_COLLECTION")
                    {
                        numeroDeColecciones++;
                    }
                    posCol++;
                    ultimaCol = words2[posCol];
                }
                posCol = 0;
                Colecciones = new string[numeroDeColecciones];
                for(int i = 0; i < numeroDeColecciones; i++)
                {
                    posCol = 0;
                    while (words2[posCol] != "DS_COLLECTION")
                    {
                        posCol++;
                    }
                    Colecciones[i] = words2[posCol + 2];
                }
                for(int j = 0; j < numeroDeColecciones; j++)
                {
                    comboBox3.Items.Add(Colecciones[j]);
                }
            }

        }


        public void Validation(string tokenr)
        {

            string NombreCol = NombreColeccion;


            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collection/" + NombreCol + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);

            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);
            string Mensaje = "";

            int num_DSCollection = 0, num_descripcion = 0;
            string NAME_COLLECTION = "", descripcion_Col ="";
            while (words2[num_DSCollection] != "DS_COLLECTION")
            {
                num_DSCollection++;
                //NAME_COLLECTION = words2[num_DSCollection];
            }
            NAME_COLLECTION = words2[num_DSCollection + 2];
            Mensaje = "Nombre de la coleccion: " + Environment.NewLine;
            Mensaje = Mensaje +  NAME_COLLECTION + Environment.NewLine;
            while (words2[num_descripcion] != "DS_DESCRIPTION")
            {
                num_descripcion++;
                //descripcion_Col = words2[num_descripcion];
            }
            descripcion_Col = words2[num_descripcion + 2];

            Mensaje = Mensaje + "Descripcion:" + Environment.NewLine;
            Mensaje = Mensaje + descripcion_Col;
            richTextBox1.Text = Mensaje;
            //richTextBox1.Text = response2.Content;
        }

        //ESTA funcion NO se usa --> era el numero de coleccion antes de que saliese el nombre de la coleccion solo salia el cd_coleccion
        public void MuestroColeccionCorrecta(string tokenr)
        {
            MessageBox.Show(comboBox2.SelectedText);
            int buscoCategoriaSeleccionada = 0;
            while (SubCategoriaNombre[buscoCategoriaSeleccionada] != comboBox2.SelectedText)
            {
                buscoCategoriaSeleccionada++;
            }

            string numeroSub = SubCategoriaNumero[buscoCategoriaSeleccionada];
            int numeroS = Int32.Parse(numeroSub);

            string url = "https://openapi.emtmadrid.es/v1/mobilitylabs/discover/collections/" + numeroS + "/";
            var client2 = new RestClient(url);
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("accessToken", tokenr);
            IRestResponse response2 = client2.Execute(request2);
            char[] delimiterChars = { /*' ', ',', '.', ':', '\t',*/ '"' };
            string text2 = response2.Content;
            string[] words2 = text2.Split(delimiterChars);

            string AcceptPetition;
            int lastCol = 0;
            string ultimaCol;
            AcceptPetition = words2[3];

            if (AcceptPetition == "00")
            {
                ultimaCol = words2[lastCol];
                while (ultimaCol != "description")
                {
                    lastCol++;
                    ultimaCol = words2[lastCol];
                }
                lastCol = lastCol - 26; //this is the position where we have the last coleccion
                //ultimaCol = words2[lastCol];

                int posCol = 0;
                while (posCol <= lastCol)
                {
                    if (ultimaCol == "CD_COLLECTION")
                    {
                        numeroDeColecciones++;
                    }
                    posCol++;
                    ultimaCol = words2[posCol];
                }
                posCol = 0;
                Colecciones = new string[numeroDeColecciones];
                for (int i = 0; i < numeroDeColecciones; i++)
                {
                    posCol = 0;
                    while (words2[posCol] != "CD_COLLECTION")
                    {
                        posCol++;
                    }
                    Colecciones[i] = words2[posCol + 2];
                }
                for (int j = 0; j < numeroDeColecciones; j++)
                {
                    comboBox3.Items.Add(Colecciones[j]);
                }
            }

        }

        }
}
