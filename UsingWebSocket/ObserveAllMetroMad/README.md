# Example Details for a BusLine in a Date

_This code is a example for make a connection from a Reactive Box Mobility Madrid 
The Reactive Box is a Meteor Server which includes many layers of data. 
Current example shows the way of make subscription at value of metroStops in METROMAD_

### Requirements 📋

_we Ccan use  Sublime Text_
_MeteorClient https://github.com/hharnisc/python-meteor_
_EventEmitter https://github.com/jfhbrook/pyee_ 
_Websockets (ws4py on https://github.com/Lawouach/WebSocket-for-Python)_
_ddpClient https://pypi.python.org/pypi/python-ddp/0.1.0_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)