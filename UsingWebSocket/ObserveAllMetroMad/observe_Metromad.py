
import time
import datetime
import json
import sys

from MeteorClient import MeteorClient

'''
This code is a example for make a connection from a Reactive Box Mobility Madrid 
The Reactive Box is a Meteor Server which includes many layers of data. 
Current example shows the way of make subscription at value of metroStops in METROMAD
    
For use this program must be include:
MeteorClient https://github.com/hharnisc/python-meteor
EventEmitter https://github.com/jfhbrook/pyee 
Websockets (ws4py on https://github.com/Lawouach/WebSocket-for-Python)
ddpClient https://pypi.python.org/pypi/python-ddp/0.1.0
'''   



def subscribed(subscription):
    print('* SUBSCRIBED {}'.format(subscription))


def unsubscribed(subscription):
    print('* UNSUBSCRIBED {}'.format(subscription))


def added(collection, id, fields):
    #print('* ADDED {} {}'.format(collection, id))
    
    if collection == "users":
        #print collection (if you want to suscribe to everyone)
        client.subscribe('METROMAD.stations.all')




    elif collection == "METROMAD.stations":
        try:
            print "item suscribed...id: "+str(id)+" values: "+str(fields)
        except:
            
             msgLog =  "Error in parameters %s" % (sys.exc_info()[1])
             print msgLog
  
             
def changed(collection, id, fields, cleared):
    
    #print ("changed:{}".format(id)+" fields: {}".format(fields))
    try:
        if collection == "METROMAD.stations":
            print "item changed...id: "+str(id)+" values: "+str(fields)
    except:
        
         msgLog =  "Error in parameters %s" % (sys.exc_info()[1])
         print msgLog
    


def connected():
    print('* CONNECTED')
    email = raw_input("email: ")
    passwd = raw_input("Password: ")
    client.login(email, passwd)

 

def subscription_callback(error):
    if error:
        print(error)

try:
    
    client = MeteorClient('ws://rbmobility.emtmadrid.es/websocket',auto_reconnect=True,auto_reconnect_timeout=5,debug=False)

    client.on('subscribed', subscribed)
    client.on('unsubscribed', unsubscribed)
    client.on('added', added)
    client.on('connected', connected)
    client.on('changed',changed)
    
    
    
    client.connect()


    # ctrl + c to kill the script
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    
   

except Exception as err :
    print err.message

