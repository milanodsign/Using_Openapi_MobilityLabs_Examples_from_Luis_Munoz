﻿using System;
using System.Diagnostics;
using DdpClient;
using DdpClient.Models;
using DdpClient.Models.Server;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace DDPClientNet.Example
{
    internal static class Program
    {
        private static DdpConnection _client;

        private class Task : DdpDocument
        {
            [JsonProperty("name")]
            public int Name { get; set; }
        }

        private static void Main(string[] args)
        {
            _client = new DdpConnection();
            _client.Login += OnLogin;
            _client.Closed += OnClose;
            _client.Error += OnError;
            _client.Connected += OnConnected;
            _client.Connect("rbmobility.emtmadrid.es");
            Console.ReadKey();
           
            DdpSubscriber<Task> subscriber = _client.GetSubscriber<Task>("BUSMADRID.flotapos");
            subscriber.Added += (o, addedModel) => Console.WriteLine("Added: " + addedModel.Object.Name);
            //subscriber.Removed += (o, removedModel) => Console.WriteLine("Removed ID: " + removedModel.Id);
            Console.ReadKey();


            _client.Close();
        }

        private static void OnError(object sender, Exception e)
        {
            Debug.WriteLine(e.Message);
            throw e;
        }

        private static void OnClose(object sender, EventArgs eventArgs)
        {
            Console.WriteLine("Closed");
        }

        private static void OnLogin(object sender, LoginResponse loginResponse)
        {
            if (loginResponse.HasError())
            {
                Console.WriteLine(loginResponse.Error.Error + ": Login error");
            }
            else {
                Console.WriteLine("Token: " + loginResponse.Token);
                Console.WriteLine("Token expires In:" + loginResponse.TokenExpires.DateTime);
            }

        }


        private static void OnConnected(object sender, ConnectResponse connectResponse)
        {
            if (connectResponse.DidFail())
            {
                Console.WriteLine("Connecting Failed" + connectResponse.Failed.Version);
            }
            else {
                Console.WriteLine("Connected! Our Session: " + connectResponse.Session);

                Console.Write("user: ");
                string user = Console.ReadLine();
                Console.Write("password: ");
                string passwd = Console.ReadLine();
                if (user.Contains("@"))
                {

                    _client.LoginWithEmail(user, passwd);
                }
                else
                {
                    _client.LoginWithUsername(user, passwd);
                }
               /* DdpSubHandler subHandler = _client.GetSubHandler("BUSMADRID.flotapos");
                subHandler.Ready += (o, args) => Console.WriteLine("Sub Ready");
                 subHandler.Sub();*/
            }
        }
    }
}