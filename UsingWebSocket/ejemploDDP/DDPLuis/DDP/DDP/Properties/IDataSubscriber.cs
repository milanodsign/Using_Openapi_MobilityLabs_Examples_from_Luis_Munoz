﻿namespace LibDDP
{
    public interface IDataSubscriber
    {
        void DataReceived(dynamic data);
        string Session { get; set; }
    }
}
