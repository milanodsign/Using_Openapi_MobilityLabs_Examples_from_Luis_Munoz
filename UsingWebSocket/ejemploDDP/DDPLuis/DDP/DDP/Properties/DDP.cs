﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibDDP
{
    public class DDP : IDataSubscriber
    {
        string strconn;
        DDPClient server;
        readonly string[] FixedFields = new string[] { "Collection", "Id", "Type", "Method" };

        public string Session { get; set; }

        public delegate void ReadyEventHandler(DDP sender, int[] ids);
        public delegate void AddedEventHandler(DDP sender, string collection, string id, dynamic data);
        public delegate void ChangedEventHandler(DDP sender, string collection, string id, string property, dynamic data);
        public delegate void ChangedCompletedEventHandler(DDP sender, string collection, string id, IDictionary<string, object> valores);
        public delegate void RemovedEventHandler(DDP sender, string collection, string id);
        public delegate void ErrorEventHandler(DDP sender, string error);
        public delegate void ConnectedEventHandler(DDP sender, string session);
        public delegate void MethodEventHandler(DDP sender, int id, dynamic result, string error);

        public event ReadyEventHandler Ready;
        public event AddedEventHandler Added;
        public event ChangedEventHandler Changed;
        public event ChangedCompletedEventHandler ChangedCompleted;
        public event RemovedEventHandler Removed;
        public event ErrorEventHandler Error;
        public event ConnectedEventHandler Connected;
        public event MethodEventHandler MethodResult;



        void OnClosed()
        {
            System.Threading.Thread.Sleep(1000);
            Reconnect();
        }
        void OnError(Exception e)
        {
            Console.WriteLine(e.Message);
            System.Threading.Thread.Sleep(1000);
            Reconnect();
        }

        public bool Reconnect()
        {
            bool res = server.Connect(strconn, false, OnClosed, OnError);
            if (res && Connected != null) Connected(this, "");
            return res;
        }

        public DDP() { server = new DDPClient(this); }
        public bool Connect(string c)
        {
            strconn = c;
            return Reconnect();
        }

        public int Call(string method, params object[] arg) { return server.Call(method, arg); }
        public int Call(string method, params string[] arg) { return server.Call(method, arg); }
        public int Subscribe(string to, params string[] args) { return server.Subscribe(to, args); }
        public int Subscribe(string to, params object[] args) { return server.Subscribe(to, args); }

        public void DataReceived(dynamic data)
        {
            try
            {
                string myProp = "Type";
                //Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(data,Formatting.Indented));
                if ((data.GetType().GetProperty(myProp) != null) ||
                    ((IDictionary<string, object>)data).ContainsKey(myProp)
                    )
                //if (data.GetType().GetProperty(myProp) != null)
                {
                    switch ((DDPType)data.Type)
                    {
                        case DDPType.Ready:
                            if (Ready != null) Ready(this, (int[])data.RequestsIds); break;
                        case DDPType.Added:
                            if (Added != null) Added(this, (string)data.Collection, Convert.ToString(data.Id), data); break;
                        case DDPType.Changed:
                            {
                                IDictionary<string, object> propertyValues = (IDictionary<string, object>)data;
                                foreach (var changed in propertyValues)
                                {
                                    if (!FixedFields.Contains(changed.Key))
                                    {
                                        if (Changed != null) Changed(this, (string)data.Collection, (string)data.Id, changed.Key, changed.Value);
                                    }
                                }
                                if (ChangedCompleted != null) ChangedCompleted(this, (string)data.Collection, (string)data.Id, propertyValues);
                            }
                            break;
                        case DDPType.Removed: if (Removed != null) Removed(this, (string)data.Collection, (string)data.Id); break;
                        case DDPType.Error: if (Error != null) Error(this, (string)data.Error); break;
                        case DDPType.Connected:
                            Session = data.Session;
                            if (Connected != null) Connected(this, (string)data.Session); break;
                        case DDPType.MethodResult: if (MethodResult != null) MethodResult(this, (int)data.id, data.result, (string)data.error); break;
                        default:
                            throw new Exception(string.Format("{0}.Error: {1}", this.ToString(), "Error decodificacion DataReceived"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}.Exception: {1} - [{2}]", this.ToString(), ex.Message, Newtonsoft.Json.JsonConvert.SerializeObject(data)));
            }
        }
    }
}
