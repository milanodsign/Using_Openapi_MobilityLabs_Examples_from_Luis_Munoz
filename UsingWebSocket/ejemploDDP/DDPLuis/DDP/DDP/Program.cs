﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibDDP 
{
    class Program : Ejemplo
    {
        private static DDP _client;

        static void Main(string[] args)
        {
            _client = new Ejemplo();
        }
    }
   
    class Ejemplo : DDP
    {
        //dynamic thing;
        string user;
        string pass;
        int loginId;
    

        // bool subscribed;

        public Ejemplo()
        {
            Connect();
            Connected += OnConnected;
            Added += OnAdded;
            Changed += OnChanged;
            Removed += OnRemoved;
            MethodResult += OnMethod;
            Ready += OnReady;
            Error += OnError;
            //subscribed = false;
        }

        private void OnError(DDP sender, string e)
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR  {0}", ex.Message);
            }

        }

        private void OnReady(DDP sender, int[] ids)
        {
            try
            {

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR  {0}", ex.Message);
            }
        }

        private void OnMethod(DDP sender, int id, dynamic result, string error)
        {
           try
            {

            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR  {0}: {1}", id, ex.Message);
            }
        }

        private void OnConnected(DDP sender, string session)
        {
            loginId = Call("login", new { password = pass, user = new { username = user } });
        }

        private void OnAdded(DDP sender, string collection, string id, dynamic data)
        {
            try
            {
                switch (collection)
                {

                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR ADDED {0}: {1}: {2}", collection, id, ex.Message);
            }
        }

        private void OnChanged(DDP sender, string collection, string id, string property, dynamic value)
        {
            try
            {
                switch (collection)
                {

                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("ERROR CHANGED {0}: {1}: {2}", collection, id, ex.Message);
            }
        }

        private void OnRemoved(DDP sender, string collection, string id)
        {
            try
            {
                switch (collection)
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR REMOVED {0}: {1}: {2}", collection, id, ex.Message);
            }
        }
        public void Connect()
        {
            string[] con = { "rbmobility.emtmadrid.es", "l          ","       " } /*= Ejemplo.Properties.Settings.Default.Split(',')*/;
            user = con[1];
            pass = con[2];
            Thread.Sleep(10000);

            Connect(con[0]);
        }

    }

}
