# ExamplesMobilityLabs

_En este proyecto estoy generando codigos de ejemplos que conectan con la API MobilityLabs de la Empresa Municipal de Transporte de Madrid (EMT) para su modelo Opendata_
_In this project, I am creating examples codes that connect with the MobilityLabs API of Empresa Municipal de Transporte de Madrid  (EMT) for his OpenData model_
### Requirements 📋

_We need download Microsoft Visual Studio_
	
	I use  Microsoft Visual Studio 2015 and framework .NET 4.6.1

_We need register in https://mobilitylabs.emtmadrid.es/ for use the Information, files, details, data, material... Moreover,if we was register, we will can do connections tests with the API_ 

_We can also use PostMan, because Postman helps us to work with the APIis_

## Authors ✒️

* **Luis Muñoz** - *Innitial project, Documentation* - [Luismu02](https://gitlab.com/Luismu02)