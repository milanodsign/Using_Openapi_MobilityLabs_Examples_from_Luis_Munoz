﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ2_Consumidor
{
    class Program
    {
        static void Main(string[] args)
        {
            //ESTO ES EL CONSUMIDOR
            string cola = Convert.ToString(args[0]);
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using(var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(cola, false, false, false, null);
                    //var consumer = new DefaultBasicConsumer(channel);new QueueingBasicConsumer(channel);
                    var consumer  = new QueueingBasicConsumer(channel); ;
                    channel.BasicConsume(cola, true, consumer);
                    Console.WriteLine("Esperando Mensajes, CTRL + c para salir...");
                    //Se deja en bucle para que este esperando mensajes
                    while (true)
                    {
                        //si hay algo en la cola lo consumo
                        var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                        //cuerpo del mensaje
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine("Recibido: {0}", message);
                    }
                }
            }
        }
    }
}
