﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RabbitMQ1.Startup))]
namespace RabbitMQ1
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
